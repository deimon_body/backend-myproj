const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();



router.get('/',function(req,res){
    res.send(UserService.getAllUsers());
})
router.get('/:id',function(req,res){
    let id = req.params.id;
    id = id.slice(1,id.length);
    const getCurrentUser = UserService.search(id);
    res.send(getCurrentUser);
})


router.put('/:id',updateUserValid,function(req,res){
    let id = req.params.id;
    id = id.slice(1,id.length);
    const newUserUpdated = UserService.updateUser(id,req.body);
    res.send(newUserUpdated);
})
router.delete('/:id',function(req,res){
    let id = req.params.id;
    id = id.slice(1,id.length);
    const deletedUser = UserService.deleteUser(id);
    res.send(deletedUser);
})
// TODO: Implement route controllers for user
router.post("/",createUserValid,function(req,res){
    const newUser = UserService.saveNewUser(req.body);
    res.send(newUser);
})

module.exports = router;