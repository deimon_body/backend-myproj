const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
    try {
        // TODO: Implement login action (get the user if it exist with entered credentials)
        const obj = {
            "email":req.body.email,
            "password":req.body.password
        }
        const data = AuthService.login(obj);
        if(!data){
            throw new Error("User not found !")
        }
        res.send(data);
    } catch (err) {
        res.status(404).send({error: true, message: err.message});
    } finally {
        next();
    }
}, responseMiddleware);


module.exports = router;